@extends('layout.master')
@section('title')
    Halaman Tambah Kategori
@endsection   
@section('subtitle')
    Tambah Kategori 
@endsection   
@section('content')


<form action="/kategori" method="POST">
    @csrf
    <div class="form-group">
        <label>Nama</label>
        <input type="text" class="form-control" name="nama" id="title" placeholder="Masukkan Title">
        @error('nama')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label>Deskripsi</label>
        <textarea name="deskripsi" class="form-control" cols="30" rows="10"></textarea>
        @error('deskripsi')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Tambah</button>
</form>

@endsection



