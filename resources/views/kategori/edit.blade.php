@extends('layout.master')
@section('title')
    Halaman Edit Kategori
@endsection   
@section('subtitle')
    Edit Kategori 
@endsection   
@section('content')


<form action="/kategori/{{$kategori->id}}" method="POST">
    @csrf
    @method('put')
    <div class="form-group">
        <label>Nama</label>
        <input type="text" class="form-control" value="{{$kategori->nama}} " name="nama" id="title" placeholder="Masukkan Title">
        @error('nama')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label>Deskripsi</label>
        <textarea name="deskripsi" class="form-control" cols="30" rows="10">{{$kategori->deskripsi}} </textarea>
        @error('deskripsi')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Update</button>
</form>

@endsection



